function startTime() {
    let today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s ;
    let t = setTimeout(startTime, 500);
  }
  function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

let col = document.getElementsByClassName("collapsible");
  let i;
  
    for (i = 0; i < col.length; i++) {
    col[i].addEventListener("click", function() {
        this.classList.toggle("active");
        let content = this.nextElementSibling;
        if (content.style.display === "block") {
        content.style.display = "none";
        } else {
        content.style.display = "block";
        }
        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        } 
    });
}


let checkBox = document.getElementById("yeah");
let mainNav = document.getElementById("main-nav");
let sideNav = document.getElementById("side-nav");    
let titleHead = document.getElementById("header");    
let logo = document.getElementById("brand");
let soc = document.getElementById("soctoggle");
let closer = document.getElementById("closer");

checkBox.addEventListener("click", mySwitcher);
console.log(checkBox);
console.log(mainNav);
console.log(logo);
console.log(titleHead);
function mySwitcher() {
    if(checkBox.checked == true){
    sideNav.style.backgroundColor = "#4d5054";
    mainNav.style.backgroundColor = "#494E56";
    titleHead.style.color = "black";
    logo.src = "assets/images/darkn.png";
    soc.style.backgroundColor = "#03989E"
    closer.style.backgroundColor = "#03989E"
    console.log(logo);
    }else{
    sideNav.style.backgroundColor = " #C4C4C4";
    mainNav.style.backgroundColor = "#E5E5E5";
    titleHead.style.color = "#494E56";
    soc.style.backgroundColor = "#494E56"
    closer.style.backgroundColor = "#494E56"
    logo.src = "assets/images/logonav.png";
    }
}